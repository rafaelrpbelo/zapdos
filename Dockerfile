FROM php:latest

ENV COMPOSER_VERSION=1.7.2 \
  CODENAME=stretch

# Update apt-get
RUN apt-get update

# Install essentials tools
RUN apt-get install -y \
  wget \
  gnupg \
  build-essential

# Installing Composer
RUN curl -sSL "https://getcomposer.org/download/$COMPOSER_VERSION/composer.phar" -o /usr/local/bin/composer && \
   chmod +x /usr/local/bin/composer

# Add postgres source
RUN echo "deb http://apt.postgresql.org/pub/repos/apt/ $CODENAME-pgdg main" >> \
  /etc/apt/sources.list.d/pgdg.list

# Add postgres repository
RUN wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | \
  apt-key add - \
  && apt-get update

# Install Postgre PDO
RUN apt-get install -y \
  libpq-dev \
  postgresql-client-9.6

RUN docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \
  && docker-php-ext-install pdo pdo_pgsql pgsql

WORKDIR /app
