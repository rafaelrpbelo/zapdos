<?php

namespace App\Controllers;

use SON\Controller\BaseController;
use App\DBConnection;
use App\Models\Post;

class IndexController extends BaseController {
  public function index() {
    $this->params->info = 'Getting posts by static function!';

    $post = new Post();
    $this->params->posts = Post::getAll();

    $this->render('index.phtml');
  }

  public function contact() {
    $param = array('info' => 'Getting posts by instance function!');

    $post = new Post();
    $this->params->posts = $post->all();

    $this->render('contact.phtml', $param, array('layout'=>'layouts/contact.phtml'));
  }
}
