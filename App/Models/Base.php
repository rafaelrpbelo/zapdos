<?php

namespace App\Models;

use App\DBConnection;

class Base
{
  protected $tableName;
  protected $db;

  public function __construct() {
    $this->db = DBConnection::getClient();
  }

  public static function getAll() {
    $className = get_called_class();
    $instance = new $className();
    $query = "SELECT * FROM " . $instance->tableName();
    return DBConnection::getClient()->query($query);
  }

  public function all() {
    $query = "SELECT * FROM " . $this->tableName();
    return $this->db->query($query);
  }

  private function tableName() {
    if (empty($this->tableName)) {
      return strtolower(str_replace("App\\Models\\", '', get_called_class()));
    } else {
      return $this->tableName;
    }
  }
}
