<?php

namespace App;

class DBConnection
{
  public static function getClient() {
    return new \PDO('pgsql:host=db;dbname=mvc', 'postgres', 'postgres');
  }
}
