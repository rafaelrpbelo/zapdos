<?php

namespace SON\Init;

abstract class Bootstrap {

  private $routes;

  public function __construct() {
    $this->initRoutes();
    $this->run($this->getUrl());
  }

  abstract protected function initRoutes();

  protected function run($url) {
    $found = false;

    array_walk($this->routes, function($route) use ($url, &$found) {
      if ($url == $route['route']) {
        $found = true;
        $class = "App\\Controllers\\" . ucfirst($route['controller']);
        $controller = new $class;
        $action = $route['action'];
        $controller->$action();
      }
    });

    if ($found == false) {
      echo "Not Found";
    }
  }

  protected function setRoutes(array $routes) {
    $this->routes = $routes;
  }

  protected function getUrl() {
    return parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
  }
}
