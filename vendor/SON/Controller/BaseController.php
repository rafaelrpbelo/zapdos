<?php

namespace SON\Controller;

abstract class BaseController {
  const VIEW_PATH = '../App/Views/';
  const DEFAULT_LAYOUT = 'layouts/application.phtml';

  protected $params;

  public function __construct() {
    $this->params = new \stdClass;
  }

  protected function render($action, $param = array(), $options = array()) {
    $options = array_merge(array('layout' => self::DEFAULT_LAYOUT), $options);

    if (!empty($options['layout'])) {
      $content = function() use ($action, $param) {
        include_once $this->getView($action);
      };

      include_once $this->getLayout($options['layout']);
    } else {
      include_once $this->getView($action);
    }
  }

  private function getLayout($layout) {
    return self::VIEW_PATH . $layout;
  }

  private function getView($action) {
    return $this->getViewPath() . $action;
  }

  private function getViewPath() {
    $actionRoot = strtolower($this->getControllerWithoutSufix());
    return self::VIEW_PATH . $actionRoot . '/';
  }

  private function getControllerWithoutSufix() {
    return str_replace('Controller', '', $this->getControllerName());
  }

  private function getControllerName() {
    return str_replace("App\\Controllers\\", '', $this->getClassName());
  }

  private function getClassName() {
    return get_class($this);
  }
}
